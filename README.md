![Build Status](https://gitlab.com/nisi_teaching/19a-itt1-programming/badges/master/pipeline.svg)


# 19A-ITT1-programming

weekly plans, resources and other relevant stuff for the 19A-ITT1-programming 1. semester autumn.

public website for students:

*  [gitlab pages](https://nisi_teaching.gitlab.io/19a-itt1-programming/)