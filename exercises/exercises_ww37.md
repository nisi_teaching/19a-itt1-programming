# Exercises for ww37/38

All exercises are using cooperative learning structures.  
Each exercise specifies which structure to use.  
The cooperative learning structures can be found from this [link](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_cooperative_learning_structures.pdf)

## Exercise 0 - Gitlab hand-in

### Information

This exercise is a quick summary of what we learned last week about interacting with Gitlab through git.

### Exercise instructions

In a group of 4 people use **circle of knowledge** to discuss your understanding of what these operations do?

1. git clone
2. git add
3. git commit
4. git push

Write your conclusions on a piece of paper.

Use 5 minutes for each operation, I will keep track of time and tell you when to switch.

\pagebreak
## Exercise 1 - Gitlab hand-in knowledge sharing

### Information

This exercise shares knowledge gathered in Exercise 0, between groups.

### Exercise instructions

Use **three for tea** to share the knowledge 

Use 5 minutes in different groups and then 5 minutes in your original groups.

I will keep track of time and tell you when to switch.

## Exercise 2 - Python for everybody chapter 1 knowledge 

### Information

This exercise recaps what you have been reading in chapter 1 of Python for everybody.

### Exercise instructions

In a group of 4 people use **Round table** to share your understanding of:

* What the Python interpreter is and how it differs from a compiler?
* What is the difference between syntax errors, logic errors and semantic errors?
* What is a program?
* What is input and output?
* What is sequential execution?
* Which 4 things should you do when debugging?

Use 5 minutes for each question, I will keep track of time and tell you when to switch.

Use **circle of knowledge** to discuss your answers

## Exercise 3 - Python for everybody chapter 2 knowledge - part 1 

### Information

This exercise recaps parts of what you have been reading in chapter 2 of Python for everybody.

### Exercise instructions

In a group of 4 people use **think-pair-share** to improve your understanding of what these operations do:

* What types of values is Python using?
* How would you check a values type in a Python program?
* What are variables?
* What are reserved words?
* What is a statement?

For each question:  
Think: 3 minutes
Pair: 2 minutes
Share: 2 minutes

## Exercise 4 - Python for everybody chapter 2 knowledge - part 2 

### Information

This exercise recaps parts of what you have been reading in chapter 2 of Python for everybody.

### Exercise instructions

use **meeting in the middle** to answer:

* What is the purpose of mnemonic naming? 
* Give at least 3 examples of mnemonic naming.

5 minutes to write answers  
10 minutes to explain  
10 minutes to discuss  

## Exercise 5 - Python for everybody chapter 2 knowledge - part 3 

### Information

This exercise recaps the rest of what you have been reading in chapter 2 of Python for everybody.

### Exercise instructions

In a group of 4 people use **think-pair-share** to improve your understanding of these questions:

* Name pythons 6 operators and their syntax
* What is order of operations?
* What is concatenation?
* How do you ask the user for input in a Python program?
* How do you insert comments in a Python program? 

For each question:  
Think: 3 minutes
Pair: 2 minutes
Share: 2 minutes

\pagebreak