# Exercises for ww44
 
Cooperative learning structures can be found from this [link](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 7 knowledge (group)

### Information

In a group of 4 people use **think-pair-share** to share your prepared knowledge about: 

* What is a file handle ?
* How do you count lines in a file ?
* How do you search through a file ?
* What does the `rstrip` method do ?
* What happens if you try to open a non existing file ?
* How do you write to a file ?


### Exercise instructions

For all question:  
Think: ? minutes
Pair: ? minutes
Share on Class: ? minutes

## Exercise 1 - Python quiz

It is important to notice your own progress when learning. This exercise will help you do that. 

### Exercise instructions

Complete the [Python quiz](https://www.w3schools.com/python/python_quiz.asp) 

You are allowed to google for answers, but remember to finish in time.  
When the quiz is done, review your answers and note the ones you couldn't answer, we will revisit the quiz later in the semester.

You have 30 minutes.  

\pagebreak

## Exercise 2 - Find your own challenge

One of the best ways to get motivated learning new things is to find something you want to accomplish, in this case by using Python.  
I want you to find somtehing you want to accomplish using Python, something you would like to be able to do!  

Don't be afraid to pick something that you dont't know how to do with Python yet, you will learn while you do it and I will help you.

Examples:  
* Read your google calendar from a python script
* Make your own electronics calculator
* Count how many lines of Python code you have written
* Track which websites you spend time visiting
* Write a game

You can also search online for challenges:  
[realpython](https://realpython.com/what-can-i-do-with-python/)  
[codementor](https://www.codementor.io/ilyaas97/6-python-projects-for-beginners-yn3va03fs)  
[codeclubprojects](https://codeclubprojects.org/en-GB/python/)

For the above i just searched google for "python projects"

You have 15 minutes to find a challenge. After 15 minutes we will do a round on class where you present your chosen challenge.

\pagebreak