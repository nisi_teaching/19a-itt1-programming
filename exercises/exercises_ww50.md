# Exercises for ww50
 
Cooperative learning structures can be found from this [link](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 13 knowledge sharing (Group) 

This exercise shares knowledge and understanding about chapter 13 in PY4E  
**Remember to write down your answers**

### Exercise instructions

In a group of 4 use think-pair-share to answer these questions:

1. How is the XML format structured?
2. How is JSON structured and what Python datatype does it resemble?
3. What Python module is used to parse JSON?
4. What is an API ?
5. Can you access all API's? if no, why?
6. What is SOA and why is it kool?
7. Give an example of combining data from 2 different API's in a single application (remember that you can both GET and POST)

Thinking: ? minutes  
Pairing: ? minutes  
Sharing: ? minutes  

## Exercise 1 - PY4E chapter 13 Knowledge sharing (Class) 

To ensure that everyone in class has the same knowledge you have to share it between groups in class.

### Exercise instructions 

1. One person from each group writes on the whiteboard
1. Use **30 for tea** to share your groups answers to questions 1 - 7

## REST API Video break

Together we will watch:

[REST API concepts and examples](https://youtu.be/7YcW25PHnAA)

## Exercise 2 - PY4E chapter 13 Exercise 1 

### Information

Use [pair programming](https://resources.collab.net/agile-101/pair-programming) to solve the first exercise from chapter 13 in PY4E

### Exercise instructions

1. Team up in new pairs
2. Download [Application 1: Google geocoding web service](https://www.py4e.com/code3/geojson.py)
3. Test and verify the application works as intended.
4. Rewrite the exercise instruction in to **Pseudo code** (logical steps in you program)  

    Exercise 1: Change geojson.py to print out the two-
    character country code from the retrieved data. Add error checking so
    your program does not traceback if the country code is not there. Once
    you have it working, search for “Atlantic Ocean” and make sure it can
    handle locations that are not in any country.

5. Write the program following the logic steps, change role for each logic step

You have ? minutes to complete the exercise

## Exercise 3 - PY4E chapter 12 Exercise 2 

### Information

This exercise requires a twitter account, if you do not have that either make one or skip to exercise 5.

Use [pair programming](https://resources.collab.net/agile-101/pair-programming) to solve the third exercise from chapter 12 in PY4E

### Exercise instructions

1. Team up in new pairs
2. Download the needed files for [Application 2: Twitter](http://www.py4e.com/code3)
3. Test and verify the application works as intended.
2. Rewrite the exercise instruction in to **Pseudo code** (logical steps in you program)
3. Write the program following the logic steps, change role for each logic step

You have ? minutes to complete the exercise

## Exercise 4 - Reading and Parsing a RSS feed

### Information

RSS feeds are available for gitlab groups, they contain information about latest commits to the project included in the group.

Use [pair programming](https://resources.collab.net/agile-101/pair-programming) to write a Python application that can read an RSS feed from gitlab.

### Exercise instructions

1. Team up in new pairs
2. Rewrite the exercise instruction in to **Pseudo code** (logical steps in you program)

    1. Install the [feedparser](https://pypi.org/project/feedparser/) module in a Python project using either PyCharm or PIP ```pip install feedparser``` depending on you IDE. Current version when writing this document is 5.2.1 
    2. import the feedparser module into you python program
    3. Go to the [EAL-ITT group](https://gitlab.com/groups/EAL-ITT/-/activity) activity page and copy the RSS feed link (look for the RSS icon). It should look something like this:  
    ```https://gitlab.com/EAL-ITT.atom?feed_token=2zPy4bfsohmsQ81hHozA```
    4. Make a variable called eal_itt and assign the RSS link to it 
    5. read the feed from gitlab using ```rss_feed = feedparser.parse(eal_itt)```
    6. print(rss_feed) to view the contents
    7. Examine the rss_feed by setting a breakpoint and debugging you program, the entries list holds the activity entries.
    8. For each of the 5 newest entries parse and print the rss_feed.entries:
        1. entry author
        2. entry updated
        3. commit message (it is in the entry summary)

        Only show the 5 newest entries 

3. Write the program following the logic steps, change role for each logic step

[feedparser documentation](https://pythonhosted.org/feedparser/)

You have ? minutes to complete the exercise

## Exercise 5 - Evaluation of the day

### Information

To retain and evaluate what we have been working with today please answer these questions individually.

Please wait for me to show up in class at 13:00   
We will finish this exercise with a class session to share your takeaways from todays lessons

### Exercise instructions

Finish these sentences using your own words:

Today i learned about.....  
The most interesting was....  
Today was particuarly good because....  
It could have been even better if....  

\pagebreak