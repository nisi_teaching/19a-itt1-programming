# Exercises for ww38/39
 
Cooperative learning structures can be found from this [link](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_cooperative_learning_structures.pdf)

## Exercise 0 - Flowchart basic symbols

### Information

In a group of 4 people use **think-pair-share** to learn about flowchart symbols.  

### Exercise instructions

Read about [flowchart basic symbols](https://www.gliffy.com/blog/how-to-flowchart-basic-symbols-part-1-of-3) and note your findings.

Read: 10 minutes  
Pair: 5 minutes  
Share: 5 minutes  

\pagebreak

## Exercise 1 - Creating flowcharts

### Information

In groups of 4 use [draw.io](https://www.draw.io/), [lucidcharts](https://www.lucidchart.com/pages/), [yED](https://www.yworks.com/products/yed) or similar to draw a flowchart of a simple IoT sensor's functionality.

### Exercise instructions

Imagine that you are building an IoT sensor that can measure temperature (Celcius), humidity (%) and barometric pressure (hPa).
The sensor is connected to the internet and responds when receiving these commands:

1. `GET_temperature_C`
2. `GET_temperature_F`
3. `GET_pressure`
4. `GET_humidity`
5. `GET_dewpoint` **hint** [what is dewpoint](https://www.omnicalculator.com/physics/dew-point#whatis)

The objective is to produce a flowchart that gives an overview of the codes flow of operation, using the correct flowchart symbols.

You do not have to show how to calculate dewpoint, but illustrate that the calculation takes place on the IoT sensor when requested.  

You have 45 minutes to complete the exercise, I will pick 2 groups to present their flowchart on class.

\pagebreak

## Exercise 2 - Python for everybody chapter 3 knowledge sharing group

### Information

This exercise recaps what you have been reading in chapter 3 of Python for everybody.

### Exercise instructions

In a group of 4 people use **Round table** to share your understanding of:

* What is the difference between conditional execution and alternative execution?
* What is the purpose of chained conditionals and what is the syntax for nested conditionals?
* What are exeptions in Python and how can you handle them ? Use examples.

Use 5 minutes for each question, I will keep track of time and tell you when to switch.

10 minutes:  
Use **circle of knowledge** to discuss and agree on your answers  
Note your agreed answers for exercise 3

\pagebreak

## Exercise 3 - Python for everybody chapter 3 knowledge sharing class

### Information

This exercise shares knowledge gathered in Exercise 2, between groups.

### Exercise instructions

Use **three for tea** to share the knowledge 

Use 5 minutes in different groups and then 5 minutes in your original groups.

If needed adjust your noted answers and share them between group members.

I will keep track of time and tell you when to switch.

\pagebreak