---
title: '19A-ITT1-programming'
subtitle: 'Exercises'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: '19A-ITT1-programming, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References
