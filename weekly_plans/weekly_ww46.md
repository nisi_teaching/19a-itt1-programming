---
Week: 46
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 46 19A-ITT1-programming - Dictionaries

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 9 exercises using pair programming 
* Read chapter 10 in Python For Everybody
* Work on your own challenge

### Learning goals

The student can:

* Implement Dictionaries
* Traverse Dictionaries
* Use Dictionary functions
* Build histograms using Dictionaries

The student knows: 

* The difference between Dictionaries and other datatypes
* When to use a Dictionary 

## Deliverables

* Chapter 9 exercises, in Python For Everybody, completed and documented on Gitlab

## Schedule

Friday 2019-11-15 (B class) Thursday 2019-11-14 (A class)

* 8:15 Introduction to the day  
* 9:00 Student presentations  
    2 groups of 2 students shows how they solved chapter 8 exercises from py4e
* 9:30 Hands-on time: On class exercises
* 11:30 Lunch break
* 12:15 Buddy work
    * Python For Everybody - Solve Chapter 9 exercises using pair programming   
    **remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 
    * Read chapter 10 in Python For Everybody 
* 13:30 Q&A session about your own challenges
* 15:30 End of day

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

PY4E Chapter 10 video lessons:

* [part 1](https://youtu.be/CaVhM65wD6g)
* [part 2](https://youtu.be/FdUdA6o0Ij0)