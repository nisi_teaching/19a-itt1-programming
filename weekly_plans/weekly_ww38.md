---
Week: 38/39
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 38/39 19A-ITT1-programming - Conditionals

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 4 in Python For Everybody

### Learning goals

The student can implement:

* Conditional execution
* logical operators
* Exception handling (try/except)
* Flowcharts

## Deliverables

* Chapter 3 exercises, in Python For Everybody, completed and documented on Gitlab

## Schedule

Thursday 2019-09-19 (B class) and Friday 2019-09-27 (A class)

* 8:15 Introduction to the day   
    OLA16 first attempt
* 8:30 Student presentations  
    2 groups of 2 students shows how they solved chapter 2 exercises from py4e
* 9:15 Hands-on time: On class exercises
* 11:30 Lunch break
* 12:15 Buddy work
    * Python For Everybody - Chapter 3 exercises   
    **remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 
    * Read chapter 4 in Python For Everybody 
* 15:30 End of day

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

Socratica videos covering some of the topics from chapter 2 and 3:

* [If, Then, Else in Python](https://youtu.be/f4KOjWS_KZs)
* [Exceptions in Python](https://youtu.be/nlCKrKGHSSk)

PY4E Chapter 4 video lessons:

* [part 1](https://youtu.be/5Kzw-0-DQAk)
* [part 2](https://youtu.be/AJVNYRqn8kM)