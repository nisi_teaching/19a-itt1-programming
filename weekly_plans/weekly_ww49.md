---
Week: 48
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 49 19A-ITT1-programming - Networked programs

## Schedule

Friday 2019-12-05 (B class) Thursday 2019-12-06 (A class)

* 8:15 Introduction to the day 
* 8:30 Student presentations  
    Students shows how they solved chapter 11 exercises from py4e
* 09:30 Hands-on time: Exercises
* 11:30 Lunch break
* 12:15 Hands-on time: Continue exercises
* 13:00 Evaluation of the day (Exercise 6)
* 13:30 Hands-on time: Continue exercises   
* 15:30 End of day

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 12 exercises 

### Learning goals

The student can:

* Use TCP sockets and libraries to get data via HTTP and HTTPS
* Develop programs using pair programming
* Reflect on own learning

The student knows: 

* Basics of TCP and HTTP(S)

## Deliverables

* Chapter 12 exercises, in Python For Everybody, completed and documented on Gitlab

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

No videos this time, we will use them in preparation next week!
