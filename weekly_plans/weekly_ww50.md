---
Week: 48
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 50 19A-ITT1-programming - Using web services

## Schedule

Friday 2019-12-12 (B class) Thursday 2019-12-13 (A class)

* 8:15 Introduction to the day 
* 8:30 Student presentations  
    Students shows how they solved chapter 12 exercises from py4e
* 9:30 Preparation for knowledge exercises  
    Either/or:
    * Read chapter 13
    * Watch chapter 13 videos 
* 10:30 Hands-on time: Knowledge exercises
* 11:30 Lunch break
* 12:15 Hands-on time: PY4E chapter exercises
* 13:00 Evaluation of the day
* 13:30 Hands-on time: continue PY4E chapter exercises
* 15:30 End of day

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 13 exercises 

### Learning goals

The student can:

* Programatically retreive JSON data
* Programatically communicate with an API
* Parse JSON data

The student knows: 

* What XML is
* What JSON is
* What web services are
* What api's are
* What a service oriented architecture is

## Deliverables

* Chapter 13 exercises, in Python For Everybody, completed and documented on Gitlab

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

PY4E Chapter 13 video lessons:

* [part 1](https://youtu.be/7NEtEctD9Cw)
* [part 2](https://youtu.be/A8KcBx9153Y)
* [part 3](https://youtu.be/0cA6W-4JPQ4)
* [part 4](https://youtu.be/J5DjteDzgoM)
* [part 5](https://youtu.be/SNeJcvBY-h4)
* [part 6](https://youtu.be/QyHcOL3C7fQ)
* [part 7](https://youtu.be/mrRo2xX39nw)

PY4E Chapter 13 worked exercises:

* [JSON](https://youtu.be/RGQfDirZ7_s)
* [GeoJSON API](https://youtu.be/vjQZscHOaG4)
* [Twitter API](https://youtu.be/zJzPyEPCbXs)


