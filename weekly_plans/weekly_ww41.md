---
Week: 41/43
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 41/43 19A-ITT1-programming - Strings

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 6 exercises using pair programming 
* Read chapter 7 in Python For Everybody
* Install PyCharm

### Learning goals

The student can implement:

* Parsing strings
* String methods
* Substrings

The student knows:

* The string type

## Deliverables

* Chapter 6 exercises, in Python For Everybody, completed and documented on Gitlab

## Schedule

Thursday 2019-10-10 (B class) and Friday 2019-10-25 (A class)

* 8:15 Introduction to the day   

* 8:30 Student presentations  
    2 groups of 2 students shows how they solved chapter 5 exercises from py4e
* 9:15 Hands-on time: On class exercises
* 10:30'ish pair programming challenge
* 11:30 Lunch break
* 12:15 Buddy work  
    * Apply for a student license and install [PyCharm](https://www.jetbrains.com/student/)
    * Python For Everybody - Solve Chapter 6 exercises using pair programming   
    **remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 
    * Read chapter 7 in Python For Everybody 
* 13:30 Q&A session with Nikolaj
* 15:30 End of day

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

PY4E Chapter 7 video lessons:

* [part 1](https://youtu.be/9KJ-XeQ6ZlI)
* [part 2](https://youtu.be/0t4rvnySKR4)