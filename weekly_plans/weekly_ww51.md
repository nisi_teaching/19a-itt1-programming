---
Week: 48
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 51 19A-ITT1-programming - Object-oriented programming

## Schedule

Friday 2019-12-21 (B class) Thursday 2019-12-20 (A class)

* 8:15 Introduction to the day 
* 8:30 Student presentations  
    Students shows how they solved chapter 13 exercises from py4e
* 9:30 Preparation  
    Either/or:
    * Read chapter 14
    * Watch chapter 14 videos 
* 10:30 Hands-on time: Exercises
* 11:30 Lunch break
* 12:15 Hands-on time: Exercises
* 15:30 End of day

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Object-oriented programming exercises 

### Learning goals

The student can:

* Create simple classes
* Instantiate objects from classes
* Extend classes

The student knows: 

* What a class is
* Why classes are useful
* Class attributes
* Class methods
* Class constructors
* The ```self``` keyword

## Deliverables

* Exercises completed and documented on gitlab

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

PY4E Chapter 14 video lessons:

* [part 1](https://youtu.be/u9xZE5t9Y30)
* [part 2](https://youtu.be/b2vc5uzUfoE)

Socratica:

* [Python Classes and Objects](https://youtu.be/apACNr7DC_s)

