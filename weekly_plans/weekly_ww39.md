---
Week: 39/40
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 39/40 19A-ITT1-programming - Functions

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 5 in Python For Everybody

### Learning goals

The student can implement:

* Functions
* Built in functions
* Math functions
* User defined functions
* Function parameters and arguments

## Deliverables

* Chapter 4 exercises, in Python For Everybody, completed and documented on Gitlab
* OLA16 hand-in on wiseflow

## Schedule

Thursday 2019-09-26 (B class) and Friday 2019-10-04 (A class)

* 8:15 Introduction to the day   

* 8:30 Student presentations  
    2 groups of 2 students shows how they solved chapter 3 exercises from py4e
* 9:15 Hands-on time: On class exercises
* 11:30 Lunch break
* 12:15 Buddy work
    * Python For Everybody - Chapter 4 exercises   
    **remember to document in your gitlab programming project with seperate .py files. Filename syntax: chX_exX.py** 
    * Read chapter 5 in Python For Everybody 
* 15:30 End of day

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_programming_exercises.pdf) for details.

## Comments

PY4E Chapter 5 video lessons:

* [part 1](https://youtu.be/FzpurxjwmsM)
* [part 2](https://youtu.be/5QDrj5ogPYc)
* [part 3](https://youtu.be/xsavQp8hd78)
* [part 4](https://youtu.be/yjlMMwf9Y5I)