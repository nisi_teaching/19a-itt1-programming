---
Week: 03
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 03 19A-ITT1-programming - Recap and evaluation

## Schedule

Thursday 2019-01-16 (A and B class)

* 8:15 Recap based on your input
* 10:00'ish QUIZ   
    It's time to try the Python quiz at [W3Schools](https://www.w3schools.com/python/python_quiz.asp) again
* 11:00'ish Evaluation  
    Please fill out this [survey](https://forms.gle/uFC6SpTUTDAmGhBC8)
* 11:30 Lunch break

## Comments

More quizzes at [realpython](https://realpython.com/quizzes/)

