---
Structure: 03
Content: Three for tea
Initials: NISI
---

# Three for tea

1. Groupmember 1 stays at the table and presents the groups solution/answers as a host for the visitors

2. The other 3 groupmembers each goes to different groups

3. The host in each group presents his/her groups solution/answers to the guests.

4. The guests thank the host and returns to their original group.

5. The groups uses **Circle of knowledge** and shares the most interesting knowledge from the other groups


**Three for tea is a rapid and efficient way to share knowledge, solutions or ideas with other groups**
