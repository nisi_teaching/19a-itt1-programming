---
Structure: 07
Content: Team interview
Initials: NISI
---

# Team interview

1. In each group one member stands up and the other member interview him/her for a fixed amount of time

2. When time is up the group member sits down and the others thanks him/her for the answers

3. Each group member is, on turn, interviewed using the procedure in step 1 and 2


* **Eveyone in the group gets attention and speaking time. This improves self-esteem**
* **The person being interviewed gets a chance to explore his/her knowledge and views**
* **Knowledge presentation based on other peoples questions and not a prepared presentation enables you to formulate knowledge in new ways**