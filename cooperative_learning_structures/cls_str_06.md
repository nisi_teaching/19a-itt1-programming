---
Structure: 06
Content: Meeting in the middle
Initials: NISI
---

# Meeting in the middle

Preperation: Each group draws a rectangle for each group member, and one common rectangle on a A3 sheet of paper.

1. One or more tasks is asked to be completed.

2. Each group member writes, within the time given, as many answers as possible in their rectangle.

3. The group uses **circle of knowledge** and each group member explains their answer.

4. The answers are discussed within the group with each member taking turns. When the group agrees on a common answer, the first group member writes a summary of the groups answers in the common rectangle. 

* **Every group member gets the opportunity to think and express their answers before the group discussion begins**
* **Skills in finding compromises and agreement are strengthened**
* **The structure sharpens the ability to express written answers in a short and precise manner**
* **When time is up the group has an output that can be stores and used for future work**
* **The visual model helps memorization**