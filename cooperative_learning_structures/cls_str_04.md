---
Structure: 04
Content: Think-pair-share
Initials: NISI
---

# Think-pair-share

1. One or more tasks is asked to be completed.

2. Each group member thinks about an answer/solution for a fixed amount of time while taking notes.

3. The answers are presented and discussed in pairs with the neighbor.

4. Group members share their combined answer/solution with the group or class

**Think-pair-share allows everybody to think before suggesting answers or solutions. The effect is amplified by first sharing their answer in pairs followed by sharing within a larger group**