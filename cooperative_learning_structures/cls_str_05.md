---
Structure: 05
Content: 30 for tea
Initials: NISI
---

# 30 for tea

Knowledge about a one or more subjects or questions has to be shared

1. The first group presents their answer. The answer is written on the white board

2. The second group presents their answer. The answer is written on the white board

3. Continue with all groups until either time is up, or there are no more answers

4. If needed answers are discussed on class

5. Each group debates if their original answer should be modified

* **It is a rapid and effecient way to share knowledge between groups**
* **The lecturer can give feedback**