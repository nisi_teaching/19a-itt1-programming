---
title: '19A ITT1 Programming'
subtitle: 'Lecture plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology programming, oeait19, 19A
* Name of lecturer and date of filling in form: NISI, 2019-06-01
* Title of the course, module or project and ECTS: Programming, 6 ECTS
* Required readings, literature or technical instructions and other background material: 
    * [Python For Everybody](https://www.py4e.com/book.php) - [(CC license)](https://creativecommons.org/licenses/by-nd/3.0/legalcode)
    * [PyCharm](https://www.jetbrains.com/pycharm/)
    * [Python Programming Tutorial](https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-) videos from Socratica


See [weekly plans](https://eal-itt.gitlab.io/19a-itt1-programming/19A_ITT1_weekly_plans.pdf) for details like detailed daily plan, links, references, exercises and so on.



| INIT | Week  | Content                                            |
|:---- |:----- |:-------------------------------------------------- |
| NISI | 36    | Introduction to programming and Python  
|      |       | Install python
|      |       | IDLE and REPL
|      |       | gitlab project for exercises
|      |       | cooperative learning structures
|      |       | 
|      |       | Preparation for week 37-38:
|      |       | Read ch. 1 and 2 in Python For Everybody
|      |       | Chapter 1 exercises in Python For Everybody
|      |       |
| NISI | 37-38 | Variables, operators, expressions and statements  
|      |       | ch. 1 exercises student presentations
|      |       | 
|      |       | Preparation for week 38-39: 
|      |       | Read ch. 3 in Python For Everybody
|      |       | Chapter 2 exercises in Python For Everybody
|      |       |
| NISI | 38-39 | Conditional execution, logical operators, flow charts, exeptions
|      |       | ch. 2 exercises student presentations
|      |       | 
|      |       | Preparation for week 39-40: 
|      |       | Read ch. 4 in Python For Everybody
|      |       | Chapter 3 exercises in Python For Everybody
|      |       | 
|      |       | OLA16 first attempt
|      |       |
| NISI | 39-40 | Functions 
|      |       | ch. 3 exercises student presentations
|      |       | PyCharm IDE - license and installation
|      |       |
|      |       | Preparation for week 40-41: 
|      |       | Read ch. 5 in Python For Everybody
|      |       | Chapter 4 exercises in Python For Everybody
|      |       |
| NISI | 40-41 | Iteration
|      |       | ch. 4 exercises student presentations
|      |       |
|      |       | Preparation for week 41-43: 
|      |       | Read ch. 6 in Python For Everybody
|      |       | Chapter 5 exercises in Python For Everybody
|      |       |
|      |       | OLA16 second attempt
|      |       |
| NISI | 41-43 | Strings
|      |       | ch. 5 exercises student presentations
|      |       |
|      |       | Preparation for week 43-44: 
|      |       | Read ch. 7 in Python For Everybody
|      |       | Chapter 6 exercises in Python For Everybody
|      |       |
| NISI | 44    | Files
|      |       | ch. 6 exercises student presentations
|      |       |
|      |       | Preparation for week 44-45: 
|      |       | Read ch. 8 in Python For Everybody
|      |       | Chapter 7 exercises in Python For Everybody
|      |       |
| NISI | 45    | Lists
|      |       | ch. 7 exercises student presentations
|      |       |
|      |       | Preparation for week 45-46: 
|      |       | Read ch. 9 in Python For Everybody
|      |       | Chapter 8 exercises in Python For Everybody
|      |       |
| NISI | 46    | Dictionaries
|      |       | ch. 8 exercises student presentations
|      |       |
|      |       | Preparation for week 47: 
|      |       | Read ch. 10 in Python For Everybody
|      |       | Chapter 9 exercises in Python For Everybody
|      |       |
| NISI | 47    | Tuples
|      |       | ch. 9 exercises student presentations
|      |       |
|      |       | Preparation for week 48: 
|      |       | Read ch. 11 in Python For Everybody
|      |       | Chapter 10 exercises in Python For Everybody
|      |       |
| NISI | 48    | Regular expressions
|      |       | ch. 10 exercises student presentations
|      |       |
|      |       | Preparation for week 49: 
|      |       | Read ch. 12 in Python For Everybody
|      |       | Chapter 11 exercises in Python For Everybody
|      |       |
| NISI | 49    | Networked programs
|      |       | ch. 11 exercises student presentations
|      |       |
|      |       | Preparation for week 50: 
|      |       | Read ch. 13 in Python For Everybody
|      |       | Chapter 12 exercises in Python For Everybody
|      |       |
| NISI | 50    | Using webservices
|      |       | ch. 12 exercises student presentations
|      |       |
|      |       | Preparation for week 51: 
|      |       | Read ch. 14 in Python For Everybody
|      |       | Chapter 13 exercises in Python For Everybody
|      |       |
| NISI | 51    | Object-oriented programming
|      |       | ch. 13 exercises student presentations
|      |       |
|      |       | Preparation for week 1: 
|      |       | Read ch. 15 in Python For Everybody
|      |       | Chapter 14 exercises in Python For Everybody
|      |       |
| NISI | 1     | Databases and SQL
|      |       | ch. 14 exercises student presentations
|      |       |
|      |       | Preparation for week 2: 
|      |       | Read ch. 16 in Python For Everybody
|      |       | Chapter 15 exercises in Python For Everybody
|      |       |
|NISI  | 2     | Visualizing data
|      |       | ch. 15 exercises student presentations
|      |       |
|      |       | Independent work: 
|      |       | Chapter 15 exercises in Python For Everybody

---------------------------------------------------------


# General info about the course, module or project


## The student’s learning outcome

Learning goals for the course are described in the curriculum section 2 about the national elements.

## Content

The subject area includes fundamentals of programming, the use of environments and data handling, as well as design, development, testing and documentation of solutions.
The course is based on the book [Python For Everybody](https://www.py4e.com/book.php) and covers the basics of programming in Python. The course is emphasized on data analysis.

## Method

The course is build using cooperative learning structures. The course uses exercises from [Python For Everybody](https://www.py4e.com/book.php) as well as exercises targeted for IoT applications.

To facilitate different learning styles the [Python Programming Tutorial](https://www.youtube.com/playlist?list=PLi01XoE8jYohWFPpC17Z-wWhPOSuh8Er-) videos from Socratica is used as supplementary material.

Exercises will be documented by students using git and gitlab as version control management.  

## Equipment

Computer capable of running Python. Windows will be used in examples and screenshots.

## Projects with external collaborators  (proportion of students who participated)
None at this time.

## Test form/assessment
The project includes 1 compulsory element.

See exam catalogue for details on compulsory elements.

## Other general information
None at this time.
