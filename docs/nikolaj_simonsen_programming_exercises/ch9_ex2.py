"""
Exercise 2: Write a program that categorizes each mail message by which day of the week the commit was done.
To do this look for lines that start with “From”, then look for the third word and keep a running
count of each of the days of the week.
At the end of the program print out the contents of your dictionary (order does not matter).

Sample Line:
From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008

Sample Execution:
python dow.py
Enter a file name: mbox-short.txt
{'Fri': 20, 'Thu': 6, 'Sat': 1}
"""

# file_name = input('Enter a file name: ')
# file = open('./files/' + file_name)

mails_dict = dict()

with open('./files/mbox-short.txt') as mails_file:
    for line in mails_file:
        if line.startswith("From "):
            linewords = line.split()
            if linewords[2] not in mails_dict:
                mails_dict[linewords[2]] = 1
            else:
                mails_dict[linewords[2]] = mails_dict[linewords[2]] + 1

print(mails_dict)



