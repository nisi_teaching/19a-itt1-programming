from simple_calculator_class import calculator

def program():
    calc = calculator()

    try:
        u_input = calc.user_input()
        calculation = ''
        operation_msg = ''

        if u_input['operation'] == 1:
            result = calc.add(u_input['number1'], u_input['number2'])
            operation_msg = result['operation_msg']
            calculation = result['calculation']
        elif u_input['operation'] == 2:
            result = calc.sub(u_input['number1'], u_input['number2'])
            operation_msg = result['operation_msg']
            calculation = result['calculation']
        elif u_input['operation'] == 3:
            result = calc.div(u_input['number1'], u_input['number2'])
            operation_msg = result['operation_msg']
            calculation = result['calculation']
        elif u_input['operation'] == 4:
            result = calc.mult(u_input['number1'], u_input['number2'])
            operation_msg = result['operation_msg']
            calculation = result['calculation']
        print(f'{u_input["number1"]} {operation_msg} {u_input["number2"]} = {calculation}')
        print('restarting....')
        program()

    except ValueError:
        print('only numbers and "done" is accepted as input, please try again')
        program()
    except ZeroDivisionError:
        print('cannot divide by zero, try again')
        program()

program()