"""
Exercise 1: Run the program on your system and see what numbers you get.
Run the program more than once and see what numbers you get.
"""

import random

print('10 random floats:\n')
for i in range(10):
    x = random.random()
    print(x)

print('\n10 random numbers between 5 and 10\n')
for i in range(10):
    x = random.randint(5, 10)
    print(x)

print('\n10 random choices between 1 and 3\n')
t = [1, 2, 3]
for i in range(10):
    print(random.choice(t))
