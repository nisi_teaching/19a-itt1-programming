class PartyAnimal:

    # attributes
    x = 0
    name = ''

    # constructor
    def __init__(self, nam):
        self.name = nam
        print(f'{self.name} was constructed')

    # methods
    def party(self):
        self.x = self.x + 1
        print(f'{self.name} so far {self.x}')

    # destructor
    def __del__(self):
        print(f'{self.name} was destructed x == {self.x}')

# main function for test purposes
def main():
    # instantiate object
    an = PartyAnimal('animal 1')

    # call object method
    an.party()
    an.party()
    an.party()

    # In this variation, we access the code from within the class and explicitly pass the
    # object pointer an as the first parameter (i.e., self within the method). You can
    # think of an.party() as shorthand for the below line.
    PartyAnimal.party(an)

    # instantiate another PartyAnimal
    an2 = PartyAnimal('animal 2')
    # call object method
    an2.party()
    an2.party()
    an2.party()
    # print the type
    print("Type", type(an2))
    # show object methods, you can see both the x integer attribute and the party method are
    # available in the object
    print("Dir ", dir(an2))
    # print class attribute type
    print("Type", type(an2.x))
    # print class method type
    print("Type", type(an2.party))

    an2 = 42
    print(f'an contains {an2}')

# only run main function if top level module
if __name__ == '__main__':
    main()