"""

Code: http://www.py4e.com/code3/db1.py

The code to create a database file and a table named Tracks with two columns in
the database is as follows:
"""

import sqlite3

# Create and connect to database
conn = sqlite3.connect('music.sqlite')
cur = conn.cursor()

# Create table in database
cur.execute('DROP TABLE IF EXISTS Tracks')
cur.execute('CREATE TABLE Tracks (title TEXT, plays INTEGER)')

cur.execute('INSERT INTO Tracks (title, plays) VALUES (?, ?)', ('Thunderstruck', 20))
cur.execute('INSERT INTO Tracks (title, plays) VALUES (?, ?)', ('My Way', 15))
conn.commit()
print('Tracks:')

cur.execute('SELECT title, plays FROM Tracks')

for row in cur:
    print(row)
cur.execute('DELETE FROM Tracks WHERE plays < 100')
conn.commit()
cur.close()

# Close database connection
conn.close()