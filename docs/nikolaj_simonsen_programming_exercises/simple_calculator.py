def add(num1, num2):
    return num1 + num2


def sub(num1, num2):
    return num1 - num2


def div(num1, num2):
    return num1 / num2


def mult(num1, num2):
    return num1 * num2


print('\nWelcome to the simple calculator (write done to quit)')

result = 0.0
chosen_operation = ''

while True:
    print('enter the first number')
    number1 = input('> ')
    if number1 == 'done':
        print('goodbye...')
        break
    print('enter the second number')
    number2 = input('> ')
    if number2 == 'done':
        print('goodbye...')
        break
    print('would like to: 1. add 2. subtract 3. divide or 4. multiply the numbers?')
    operation = input('> ')
    if operation == 'done':
        print('goodbye...')
        break

    try:
        number1 = float(number1)
        number2 = float(number2)
        operation = int(operation)

        if operation == 1:
            result = add(number1, number2)
            chosen_operation = ' added with '
        elif operation == 2:
            result = sub(number1, number2)
            chosen_operation = ' subtracted from '
        elif operation == 3:
            result = div(number1, number2)
            chosen_operation = ' divided with '
        elif operation == 4:
            result = mult(number1, number2)
            chosen_operation = ' multiplied with '

        print(str(number1) + chosen_operation + str(number2) + ' = ' + str(result))
        print('restarting....')

    except ValueError:
        print('only numbers and "done" is accepted as input, please try again')

    except ZeroDivisionError:
        print('cannot divide by zero, please try again')
