# Obligatory Learning Activity 16 (OLA16)

### Course: Programming
### Class: OEAIT19EIA and OEAIT19EIB

## Information

For rules and regulations about OLA, see the semester description on itslearning, in the folder, ressources/general education documents.

## Exercise code instructions

The exercise code to be completed is from chapter 3, exercise 3 in Python for everybody.

1. **Write a Python program to prompt for a score between 0.0 and 1.0.  
If the score is out of range, print an error message.  
If the score is between 0.0 and 1.0, print a grade using the following table:**

    | Score    | Grade |  
    | -------- | ----- |
    | `>= 0.9` | `A`   |  
    | `>= 0.8` | `B`   |
    | `>= 0.7` | `C`   |
    | `>= 0.6` | `D`   |
    | `<  0.6` | `F`   |

2. **Run the program repeatedly as shown below to test the various different values for input.**

    Enter score: 0.95  
    A  
    Enter score: perfect  
    Bad score  
    Enter score: 10.0  
    Bad score  
    Enter score: 0.75  
    C  
    Enter score: 0.5  
    F  


<div class="page"/>



## Hand-in on wiseflow

1. A .py file containing: 

    a. your full name as a comment  
    b. your class number as a comment  
    c. the exercise solution code 

2. A small report containing:

    a. A small introduction
    b. Exercise solution code  
    c. Screen shots of the different input tests as described in the instructions   
    d. A flowchart of the exercise solution code (embedded, no links)

    **Use the file "19S Report and assignment document standard PDA V01.docx" attached to wiseflow as extra material** 