---
title: 'ITT2 Project'
subtitle: 'Group list'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: 'ITT2 project, group list'
---

Introduction
====================

This document is compiled from the file `groups.txt` which includes the list of groups, and the file `group.md` from each repository.

The template for the file is

```
Group: <name>
-----------------

Members:

* <member A> <riot username>
* <member B> <riot username>
* <member C> <riot username>

Juniper router: 

* Management SSH: <ip>:<port>
* External ip: <ip>

Raspberry 

* SSH access: <ip>:<port>
* REST API access: [http://<ip>:<port>](http://<ip>:<port>)

Group web server: 

* SSH access: <ip>:<port>
* REST API access: [http://<ip>:<port>](http://<ip>:<port>)



```

Newlines and specific formatting is important, and it is not allowed to add extra stuff.


Group list
================





